


### 伤寒大白 之 口唇诊
	#口中干湿可定其症之表里轻重。

	#口唇关手足阳明肠胃二经，又关手足太阴脾肺二脏。

	#唇色红润，里未有热，宜辛温散表。

	#唇色干枯，里已有热，宜清里。

	#唇色焦黑
		#如烦渴消水，里热已极，当用凉膈散等。
		#如谵语发狂，唇色干焦，服寒凉而热不减，此食滞中焦，胃中蕴蓄，发黄发热。是以用凉药。则食滞不消，用辛散则碍里热，宜以保和散冲竹沥、芦卜汁，或
		汤加陈枳实治之。

	#上唇属肺与大肠
		#上唇焦而消渴饮水，热在上，主肺；
		#上唇焦而不消渴饮水，热在下，主大肠有燥粪。

	#下唇属脾与胃
		#下唇焦而消渴饮水，热在阳明胃；
		#下唇焦而不消渴饮水，热在太阴脾。

	#唇焦主里热、食滞。
		余今发明里热唇焦，食滞唇焦，积热伏于血分而唇焦，惟以渴不渴、消水不消水分别。
	
	#唇焦：
		或食滞：
			脉若滑大不数，食未蒸热，口亦不渴；
			脉若滑大沉数，食已发热，口亦作渴。
			故凡谵语发狂；脉滑不数，渴不消水者，亦以食滞治之；
		或里热：
		或热积血分：





### 喉相关
	#咽喉乾燥为有热且缺失津液，不可發汗。如出疹痘最後餘熱不盡回到嗓子，這時發汗非發汗封喉会要命。但不以嗓子疼為主的表证，可用桂枝汤+桔梗或葛根汤+桔梗。